<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Process\Process;
use App\Message\OrderSummary;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class OrderSummaryCommand extends Command
{
    protected static $defaultName = 'app:order-summary';
    
    protected $bus;
    private $router;

    public function __construct(MessageBusInterface $bus, RouterInterface $router)
    {
        parent::__construct();
        $this->bus = $bus;
        $this->router = $router;
    }

    protected function configure(): void
    {
        $this->addArgument('url', InputArgument::REQUIRED, 'json url?');
    }

    protected function downloadJsonFromUrl($url) 
    {
        try {
            $stream = stream_context_create([
                'http' => [
                    'method' => 'GET'
                ]
            ]);
        
            $fp = fopen($url, 'r', false, $stream);
            
            $content = stream_get_contents($fp);
            return explode("\n", $content);
        } catch (\Exception $e) {
            throw new \Exception('Invalid URL');
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int{
        try {
            $url = $input->getArgument('url');
            $orders = $this->downloadJsonFromUrl($url);
            $progressBar = new ProgressBar($output, count($orders) * 2);
            $last = 0;
            foreach($orders as $row) {
                if ($data = $this->validateJson($row)) {
                    $this->bus->dispatch(new OrderSummary($row));
                    $progressBar->advance();
                    $last = $data->order_id;
                }
            }
            if ($last > 0) {
                $this->watcher($progressBar, $last);
            }

            $url = $this->router->generate('order_summary_report', [], UrlGeneratorInterface::ABSOLUTE_URL);
            $progressBar->finish();
            $output->writeln("\n<href={$url}>Download Report</>");
            return Command::SUCCESS;
        } catch (\Exception $e) {
            $output->writeln("FAILURE: {$e->getMessage()}");
            return Command::FAILURE;
        }

    }

    protected function watcher($progressBar, $last) {
        $process = new Process(['php', 'bin/console', 'messenger:consume']);
        $process->start();

        $keyToFinish = "FINISH order_id: {$last}";
        foreach ($process as $type => $data) {
            if ($process::OUT === $type && str_contains($data, $keyToFinish)) {
                $progressBar->advance();
                break;
            }
        }
    }

    protected function validateJson($line)
    {
        if (is_string($line)) {
            $data = json_decode($line);
            if (json_last_error() !== JSON_ERROR_NONE || !isset($data->order_id)) {
                return false;
            }
            return $data;
        }
    }
}