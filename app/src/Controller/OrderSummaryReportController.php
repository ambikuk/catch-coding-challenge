<?php

namespace App\Controller;
use App\Repository\OrderSummaryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use League\Csv\Writer;
use NumberFormatter;

class OrderSummaryReportController extends AbstractController
{
    #[Route('/order/summary/report/csv', name: 'order_summary_report')]
    public function csv(OrderSummaryRepository $repository): Response
    {
        $data = $repository->findAll();
        $records = $this->mappingRecords($data);
        $file = $this->generateCSV($records);

        return new Response($file->content, 200, [
            'Content-Encoding' => 'none',
            'Content-Type' => 'text/csv; charset=UTF-8',
            'Content-Disposition' => "attachment; filename={$file->name}.csv",
            'Content-Description' => 'Order Summary Report',
        ]);
    }

    protected function generateCSV ($records) {
        $header = [
            'order_id',
            'order_datetime', 
            'total_order_value', 
            'average_unit_price', 
            'distinct_unit_count', 
            'total_units_count', 
            'customer_state'
        ];
        $currentTime = date('Y-m-d H:i:s');
        $name = "report-summary-{$currentTime}.csv";
        $writer = Writer::createFromString('');
        $writer->insertOne($header);
        $writer->insertAll($records); 
        return (object) [
            'name' => $name,
            'content' => $writer->getContent()
        ];
    }

    protected function mappingRecords($data = [])
    {
        return array_map(function ($row) {
            $formatter = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
            return [
                'order_id' => $row->getOrderId(),
                'order_datetime' => $row->getOrderDatetime()->format(\DateTime::ATOM),
                'total_order_value' => $row->getTotalOrderValue(),
                'average_unit_price' => $formatter->formatCurrency($row->getAverageUnitPrice(), 'USD'),
                'distinct_unit_count' => $row->getDistinctUnitCount(),
                'total_units_count' => $row->getTotalUnitsCount(),
                'customer_state' => $row->getCustomerState(),
            ];
        }, $data);
    }
}
