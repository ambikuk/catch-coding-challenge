<?php

namespace App\Service;

class OrderSummaryService extends OrderService
{
    private $totalOrderValue;
    private $totalItemPrice;
    private $distintUnit = [];
    private $totalUnit;
    
    public function __construct($data = [])
    {
        $this->setAttributes($data);
        $this->summary();
    }

    protected function summary()
    {
        foreach ($this->items as $item) {
            $this->totalOrderValue += $item->quantity * $item->unit_price;
            $this->totalUnit += $item->quantity;
            $this->totalItemPrice += $item->unit_price;
            array_push($this->distintUnit, $item->product->product_id);
        }
    }

    protected function discount($totalOrder)
    {
        $discounts = $this->discounts;
        usort($discounts, function ($a, $b) {
            return $a->priority <=> $b->priority;
        });

        foreach ($discounts as $discount) {
            if ($discount->type === "DOLLAR") {
                $totalOrder = $totalOrder - $discount->value;
            }
            if ($discount->type === "PERCENTAGE") {
                $totalOrder = $totalOrder - ($totalOrder * $discount->value / 100);
            }
        }
        return $totalOrder;
    }

    public function getOrderId()
    {
        return $this->order_id;
    }

    public function getOrderDate()
    {
        return new \DateTime($this->order_date);
    }

    public function getTotaOrderValue()
    {
        return $this->discount($this->totalOrderValue);
    }

    public function getAverageUnitPrice()
    {
        return $this->totalOrderValue / $this->getTotalUnitCount();
    }

    public function getTotalUnitCount()
    {
        return $this->totalUnit;
    }

    public function getDistintUnitCount()
    {
        return count($this->distintUnit);
    }

    public function getCustomerState()
    {
        return $this->customer->shipping_address->state;
    }

    public function getContent()
    {
        return [
            'order_id' => $this->getOrderId(),
            'order_datetime' => $this->getOrderDate(),
            'total_order_value' => $this->getTotaOrderValue(),
            'average_unit_price' => $this->getAverageUnitPrice(),
            'distinct_unit_count' => $this->getDistintUnitCount(),
            'total_units_count' => $this->getTotalUnitCount(),
            'customer_state' => $this->getCustomerState(),
        ];
    }


}