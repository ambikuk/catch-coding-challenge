<?php

namespace App\Service;


class OrderService
{

    protected $order_id;
    protected $order_date;
    protected $customer;
    protected $items;
    protected $discounts;
    protected $shipping_price;

    public function setAttributes($data = [])
    {
        $this->order_id = $data['order_id'];
        $this->order_date = $data['order_date'];
        $this->customer = $data['customer'];
        $this->items = $data['items'];
        $this->discounts = $data['discounts'];
        $this->shipping_price = $data['shipping_price'];
    }
}