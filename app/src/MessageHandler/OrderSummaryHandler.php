<?php

namespace App\MessageHandler;

use App\Message\OrderSummary;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use App\Service\OrderSummaryService;
use App\Repository\OrderSummaryRepository;
use App\Entity\OrderSummary as Entity;
use Exception;
use Psr\Log\LoggerInterface;

#[AsMessageHandler]
class OrderSummaryHandler
{

    private $orderSummaryRepository;

    public function __construct(OrderSummaryRepository $orderSummaryRepository)
    {
        $this->orderSummaryRepository = $orderSummaryRepository;
    }

    public function __invoke(OrderSummary $messsage)
    {
        $data = json_decode($messsage->getContent());
        if (!is_object($data)) {
            throw new \Exception('data invalid!');
        }

        try {
            $service = new OrderSummaryService([
                'order_id' => $data->order_id,
                'order_date' => $data->order_date,
                'customer' => $data->customer,
                'items' => $data->items,
                'discounts' => $data->discounts,
                'shipping_price' => $data->shipping_price,
            ]);
            
            $entity = new Entity();
            $entity->setOrderId($service->getOrderId());
            $entity->setOrderDatetime($service->getOrderDate());
            $entity->setTotalOrderValue($service->getTotaOrderValue());
            $entity->setAverageUnitPrice($service->getAverageUnitPrice());
            $entity->setDistinctUnitCount($service->getDistintUnitCount());
            $entity->setTotalUnitsCount($service->getTotalUnitCount());
            $entity->setCustomerState($service->getCustomerState());
    
            $this->orderSummaryRepository->save($entity);
        } catch (\Exception $e) {
            echo "SKIP order_id: {$service->getOrderId()} - ";
        }

        echo "FINISH order_id: {$service->getOrderId()}";
    }
}