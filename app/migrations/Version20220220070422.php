<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220220070422 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE order_summary_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE order_summary (id INT NOT NULL, order_id INT NOT NULL, order_datetime TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, total_order_value DOUBLE PRECISION NOT NULL, average_unit_price DOUBLE PRECISION NOT NULL, distinct_unit_count INT NOT NULL, total_units_count INT NOT NULL, customer_state VARCHAR(150) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3852CF288D9F6D38 ON order_summary (order_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE order_summary_id_seq CASCADE');
        $this->addSql('DROP TABLE order_summary');
    }
}
