<?php 
namespace App\Tests\Controller;

use DateTime;
use App\Repository\OrderSummaryRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OrderSummaryReportControllerTest extends WebTestCase
{
    public function testOrderSummaryReport()
    {
        $client = static::createClient();
        $client->request('GET', '/order/summary/report/csv');
        $this->assertResponseIsSuccessful();
    }
}