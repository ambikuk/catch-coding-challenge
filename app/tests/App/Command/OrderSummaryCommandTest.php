<?php

namespace App\Tests\App\Command;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

use App\Command\OrderSummaryCommand;

class OrderSummaryCommandTest extends KernelTestCase
{
    private $command;
    private $bus;
    private $router;

    protected function setUp(): void
    {
        $this->bus = $this->getMockBuilder('Symfony\Component\Messenger\MessageBusInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $this->router = $this->getMockBuilder('Symfony\Component\Routing\RouterInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new OrderSummaryCommand($this->bus, $this->router));
        $this->command = $application->find('app:order-summary');
    }
    
    public function testExecuteWrongUrl(): void
    {
    
        $command = $this->command;
        $commandTester = new CommandTester($command);
        $commandTester->execute(['url' => 'wrong url']);

        $this->assertMatchesRegularExpression('/FAILURE: Invalid URL/', $commandTester->getDisplay());
    }

    public function testExecuteWrongContent(): void
    {
        $command = $this->command;
        $commandTester = new CommandTester($command);
        $commandTester->execute(['url' => 'https://www.w3.org/Provider/Style/dummy.html']);

        $this->assertMatchesRegularExpression('/=====/', $commandTester->getDisplay());
    }
}
