<?php 
namespace App\Tests\Repository;

use App\Entity\OrderSummary;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Entity\OrderSummary as Entity;

class ProductRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $query = $this->entityManager->createQuery('DELETE App\Entity\OrderSummary os WHERE os.order_id = :orderId')
            ->setParameter('orderId', 1001);

        $query->execute();
    }

    public function testSave()
    {
        $new = new Entity();
        $new->setOrderId(1001);
        $new->setOrderDatetime(new \DateTime('Fri, 15 Mar 2019 08:56:57 +0000'));
        $new->setTotalOrderValue(359.78);
        $new->setAverageUnitPrice(59.96);
        $new->setDistinctUnitCount(2);
        $new->setTotalUnitsCount(6);
        $new->setCustomerState('VICTORIA');
        $save = $this->entityManager->getRepository(OrderSummary::class);
        $save->save($new);

        $find = $this->entityManager
            ->getRepository(OrderSummary::class)
            ->findOneBy(['order_id' => 1001]);

        $this->assertSame(1001, $find->getOrderId());
        $this->assertEquals(new \DateTime('Fri, 15 Mar 2019 08:56:57 +0000'), $find->getOrderDatetime());
        $this->assertSame(359.78, $find->getTotalOrderValue());
        $this->assertSame(59.96, $find->getAverageUnitPrice());
        $this->assertSame(2, $find->getDistinctUnitCount());
        $this->assertSame(6, $find->getTotalUnitsCount());
        $this->assertSame('VICTORIA', $find->getCustomerState());
        $this->assertTrue(is_int($find->getId()));
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}