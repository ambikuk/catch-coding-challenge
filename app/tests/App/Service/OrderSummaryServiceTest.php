<?php

namespace App\Tests\App\Service;

use App\Service\OrderSummaryService;
use PHPUnit\Framework\TestCase;

class OrderSummaryServiceTest extends TestCase
{
    protected $sample = [];

    protected function setUp() : void
    {
        $this->sample = [
            'order_id' => '1120',
            'order_date' => 'Fri, 15 Mar 2019 08:56:57 +0000',
            'customer' => (Object) [
                'customer_id' => '5220760',
                'first_name' => 'Kevin',
                'last_name' => 'Runolfsdottir',
                'email' => 'kevin.runolfsdottir@example.com',
                'phone' => '08 1332 5883',
                'shipping_address' => (Object) [
                    'street' => '69 AUSTRAL ST',
                    'postcode' => '2036',
                    'suburb' => 'MALABAR',
                    'state' => 'NEW SOUTH WALES'
                ],
            ],
            'items' =>  [ 
                (Object) [
                'quantity' => '2',
                'unit_price' => '20',
                'product' => (object) [
                    'product_id' => '3554808',
                    'title' => 'Tangle Teezer Compact Styler Detangling Hairbrush - Skinny Dip/Flamingo',
                    'subtitle' => '',
                    'image' => 'https://s.catch.com.au/images/product/0020/20149/5cd16629065f5118438549.jpg',
                    'thumbnail' => 'https://s.catch.com.au/images/product/0020/20149/5cd16629065f5118438549_w200.jpg',
                    'category' => ['BEAUTY', 'HAIR CARE', 'HAIR STYLING TOOL ACCESSORIES', 'HAIR BRUSH'],
                    'url' => 'https://www.catch.com.au/product/tangle-teezer-compact-styler-detangling-hairbrush-skinny-dip-flamingo-3554808',
                    'upc' => '5060173370671',
                    'gtin14' => '',
                    'created_at' => '2019-01-22 14:23:15',
                    'brand' => (object) [
                        'id' => '8236',
                        'name' => 'Tangle Teezer'
                    ]
    
                ]], 
                (Object) [
                'quantity' => '2',
                'unit_price' => '40',
                'product' => (object) [
                    'product_id' => '3760377',
                    'title' => 'XTEND Pro Whey Protein Isolate Chocolate Lava Cake 826g',
                    'subtitle' => '',
                    'image' => 'https://s.catch.com.au/images/product/0018/18212/5c902cf1cfad4642725011.jpg',
                    'thumbnail' => 'https://s.catch.com.au/images/product/0018/18212/5c902cf1cfad4642725011_w200.jpg',
                    'category' => ['HEALTH', 'DIET & FITNESS', 'FITNESS', 'PROTEIN POWDERS'],
                    'url' => 'https://www.catch.com.au/product/xtend-pro-whey-protein-isolate-chocolate-lava-cake-826g-3760377',
                    'upc' => '842595110982',
                    'gtin14' => '',
                    'created_at' => '2019-03-04 15:18:28',
                    'brand' =>  (object) [
                        'id' => '7104',
                        'name' => 'Scivation'
                    ]
                ]
            ]],
            'discounts' => [],
            'shipping_price' => '10'
        ];
        parent::setUp();
    }

    public function testOrderSummaryService(): void
    {
        
        $orderSummary = new OrderSummaryService($this->sample);

        $this->assertEquals([
            'order_id' => '1120',
            'order_datetime' => new \DateTime('Fri, 15 Mar 2019 08:56:57 +0000'),
            'total_order_value' => 120,
            'average_unit_price' => 30,
            'distinct_unit_count' => 2,
            'total_units_count' => 4,
            'customer_state' => 'NEW SOUTH WALES',
        ], $orderSummary->getContent());
    }
    
    public function testOrderSummaryServiceWithDiscount(): void
    {
        $sample = $this->sample;
        $sample['discounts'] = [
            (object) ['type' => 'DOLLAR', 'value' => '3', 'priority' => '2'],
            (object) ['type' => 'PERCENTAGE', 'value' => '10', 'priority' => '1']
        ];
        $orderSummary = new OrderSummaryService($sample);

        $this->assertEquals([
            'order_id' => '1120',
            'order_datetime' => new \DateTime('Fri, 15 Mar 2019 08:56:57 +0000'),
            'total_order_value' => 105,
            'average_unit_price' => 30,
            'distinct_unit_count' => 2,
            'total_units_count' => 4,
            'customer_state' => 'NEW SOUTH WALES',
        ], $orderSummary->getContent());
    }
}
