<?php

namespace App\Tests\App\Message;

use App\Message\OrderSummary;
use PHPUnit\Framework\TestCase;

class OrderSummaryTest extends TestCase
{
    public function testOrderSummaryMassage(): void
    {
        $sample = 'string';
        $orderSummary = new OrderSummary($sample);

        $this->assertEquals($orderSummary->getContent(), $sample);
    }
}
