# Catch Coding Challenge

Buhori D

Email: buhoridermawan@gmail.com

## Installation

Setup application
```bash
git clone https://ambikuk@bitbucket.org/ambikuk/catch-coding-challenge.git
cd docker
docker-compose up -d
```
Make sure composer has been updated
```bash
docker-compose exec php-fpm composer update
```
DB Migration
```bash
docker-compose exec php-fpm php bin/console doctrine:database:create
docker-compose exec php-fpm php bin/console doctrine:database:create --env=test
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate --env=test
```

## Usage
```bash
docker-compose exec php-fpm php bin/console app:order-summary https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl
```

## TEST
```bash
docker-compose exec php-fpm composer test:coverage
```


